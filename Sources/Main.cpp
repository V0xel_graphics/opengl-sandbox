#include "pch.h"
#include "Window.h"
#include "Shader.h"
#include "Mesh.h"
#include "Textures.h"

void framebufferSizeCallback(GLFWwindow *window, const s32 width, const s32 height);
void getInput(GLFWwindow *window);

f32 PosZ = -3.0f;

s32 WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance,
	PSTR lpCmdLine, INT nCmdShow)
{
	// HGLRC contextPtr = wglGetCurrentContext(); //windows only
	Window wnd;
	if (!gladLoadGLLoader((GLADloadproc)glfwGetProcAddress))
		throw std::runtime_error("Failed to initialize GLAD");

	glViewport(0, 0, wnd.width, wnd.height); //telling openGl size for rendering
	glfwSetFramebufferSizeCallback(wnd.wndPtr, framebufferSizeCallback); //set callback func for resizing

	// Inputs, Raw Data
	const std::vector<glm::vec3>cubeVerts = {
		glm::vec3{-0.5f, -0.5f, -0.5f},
		glm::vec3{ 0.5f, -0.5f, -0.5f},
		glm::vec3{ 0.5f,  0.5f, -0.5f},
		glm::vec3{ 0.5f,  0.5f, -0.5f},
		glm::vec3{-0.5f,  0.5f, -0.5f},
		glm::vec3{-0.5f, -0.5f, -0.5f},
		
		glm::vec3{-0.5f, -0.5f,  0.5f},
		glm::vec3{ 0.5f, -0.5f,  0.5f},
		glm::vec3{ 0.5f,  0.5f,  0.5f},
		glm::vec3{ 0.5f,  0.5f,  0.5f},
		glm::vec3{-0.5f,  0.5f,  0.5f},
		glm::vec3{-0.5f, -0.5f,  0.5f},
		
		glm::vec3{-0.5f,  0.5f,  0.5f},
		glm::vec3{-0.5f,  0.5f, -0.5f},
		glm::vec3{-0.5f, -0.5f, -0.5f},
		glm::vec3{-0.5f, -0.5f, -0.5f},
		glm::vec3{-0.5f, -0.5f,  0.5f},
		glm::vec3{-0.5f,  0.5f,  0.5f},
	
		glm::vec3{ 0.5f,  0.5f,  0.5f},
		glm::vec3{ 0.5f,  0.5f, -0.5f},
		glm::vec3{ 0.5f, -0.5f, -0.5f},
		glm::vec3{ 0.5f, -0.5f, -0.5f},
		glm::vec3{ 0.5f, -0.5f,  0.5f},
		glm::vec3{ 0.5f,  0.5f,  0.5f},
	
		glm::vec3{-0.5f, -0.5f, -0.5f},
		glm::vec3{ 0.5f, -0.5f, -0.5f},
		glm::vec3{ 0.5f, -0.5f,  0.5f},
		glm::vec3{ 0.5f, -0.5f,  0.5f},
		glm::vec3{-0.5f, -0.5f,  0.5f},
		glm::vec3{-0.5f, -0.5f, -0.5f},
	
		glm::vec3{-0.5f,  0.5f, -0.5f},
		glm::vec3{ 0.5f,  0.5f, -0.5f},
		glm::vec3{ 0.5f,  0.5f,  0.5f},
		glm::vec3{ 0.5f,  0.5f,  0.5f},
		glm::vec3{-0.5f,  0.5f,  0.5f},
		glm::vec3{-0.5f,  0.5f, -0.5f}
						   
	};					   

	const std::vector<glm::vec3>cubeColors = {
		glm::vec3{1.0f, 0.0f, 0.0f},
		glm::vec3{0.0f, 1.0f, 0.0f},
		glm::vec3{0.0f, 0.0f, 1.0f},
		glm::vec3{0.0f, 0.0f, 1.0f},
		glm::vec3{1.0f, 1.0f, 0.0f},
		glm::vec3{1.0f, 0.0f, 0.0f},

		glm::vec3{1.0f, 0.0f, 0.0f},
		glm::vec3{0.0f, 1.0f, 0.0f},
		glm::vec3{0.0f, 0.0f, 1.0f},
		glm::vec3{0.0f, 0.0f, 1.0f},
		glm::vec3{1.0f, 1.0f, 0.0f},
		glm::vec3{1.0f, 0.0f, 0.0f},

		glm::vec3{1.0f, 0.0f, 0.0f},
		glm::vec3{0.0f, 1.0f, 0.0f},
		glm::vec3{0.0f, 0.0f, 1.0f},
		glm::vec3{0.0f, 0.0f, 1.0f},
		glm::vec3{1.0f, 1.0f, 0.0f},
		glm::vec3{1.0f, 0.0f, 0.0f},

		glm::vec3{1.0f, 0.0f, 0.0f},
		glm::vec3{0.0f, 1.0f, 0.0f},
		glm::vec3{0.0f, 0.0f, 1.0f},
		glm::vec3{0.0f, 0.0f, 1.0f},
		glm::vec3{1.0f, 1.0f, 0.0f},
		glm::vec3{1.0f, 0.0f, 0.0f},

		glm::vec3{1.0f, 0.0f, 0.0f},
		glm::vec3{0.0f, 1.0f, 0.0f},
		glm::vec3{0.0f, 0.0f, 1.0f},
		glm::vec3{0.0f, 0.0f, 1.0f},
		glm::vec3{1.0f, 1.0f, 0.0f},
		glm::vec3{1.0f, 0.0f, 0.0f},

		glm::vec3{1.0f, 0.0f, 0.0f},
		glm::vec3{0.0f, 1.0f, 0.0f},
		glm::vec3{0.0f, 0.0f, 1.0f},
		glm::vec3{0.0f, 0.0f, 1.0f},
		glm::vec3{1.0f, 1.0f, 0.0f},
		glm::vec3{1.0f, 0.0f, 0.0f},
	};

	const std::vector<glm::vec2>cubeTexCoords = {
		glm::vec2{0.0f, 0.0f},
		glm::vec2{1.0f, 0.0f},
		glm::vec2{1.0f, 1.0f},
		glm::vec2{1.0f, 1.0f},
		glm::vec2{0.0f, 1.0f},
		glm::vec2{0.0f, 0.0f},
		
		glm::vec2{0.0f, 0.0f},
		glm::vec2{1.0f, 0.0f},
		glm::vec2{1.0f, 1.0f},
		glm::vec2{1.0f, 1.0f},
		glm::vec2{0.0f, 1.0f},
		glm::vec2{0.0f, 0.0f},
	
		glm::vec2{1.0f, 0.0f},
		glm::vec2{1.0f, 1.0f},
		glm::vec2{0.0f, 1.0f},
		glm::vec2{0.0f, 1.0f},
		glm::vec2{0.0f, 0.0f},
		glm::vec2{1.0f, 0.0f},

		glm::vec2{1.0f, 0.0f},
		glm::vec2{1.0f, 1.0f},
		glm::vec2{0.0f, 1.0f},
		glm::vec2{0.0f, 1.0f},
		glm::vec2{0.0f, 0.0f},
		glm::vec2{1.0f, 0.0f},

		glm::vec2{0.0f, 1.0f},
		glm::vec2{1.0f, 1.0f},
		glm::vec2{1.0f, 0.0f},
		glm::vec2{1.0f, 0.0f},
		glm::vec2{0.0f, 0.0f},
		glm::vec2{0.0f, 1.0f},

		glm::vec2{0.0f, 1.0f},
		glm::vec2{1.0f, 1.0f},
		glm::vec2{1.0f, 0.0f},
		glm::vec2{1.0f, 0.0f},
		glm::vec2{0.0f, 0.0f},
		glm::vec2{0.0f, 1.0f}
		
	};	

	const std::vector<u32>cubeIndices = {
		0, 1, 3, 3, 1, 2,
		1, 5, 2, 2, 5, 6,
		5, 4, 6, 6, 4, 7,
		4, 0, 7, 7, 0, 3,
		3, 2, 7, 7, 2, 6,
		4, 5, 0, 0, 5, 1
	};

	const std::vector<glm::vec3>cubePositions = {
	  glm::vec3{0.0f,  0.0f,  0.0f},
	  glm::vec3{2.0f,  5.0f, -15.0f},
	  glm::vec3{-1.5f, -2.2f, -2.5f},
	  glm::vec3{-3.8f, -2.0f, -12.3f},
	  glm::vec3{2.4f, -0.4f, -3.5f},
	  glm::vec3{-1.7f,  3.0f, -7.5f},
	  glm::vec3{1.3f, -2.0f, -2.5f},
	  glm::vec3{1.5f,  2.0f, -2.5f},
	  glm::vec3{1.5f,  0.2f, -1.5f},
	  glm::vec3{-1.3f,  1.0f, -1.5}
	};

	// Compiling shaders and linking them to OpenGL program
	const Shader shader(
		"D:/programowanie/MojeNowe/Gl_sandbox/Sources/Shaders/VertexShader.vert", 
		"D:/programowanie/MojeNowe/Gl_sandbox/Sources/Shaders/FragmentShader.frag");
	const Shader shader2(
		"D:/programowanie/MojeNowe/Gl_sandbox/Sources/Shaders/VertexShader.vert", 
		"D:/programowanie/MojeNowe/Gl_sandbox/Sources/Shaders/FragmentShader2.frag");

	// Objects creation, filling buffers
	const VerticesData3f cubeVBO(cubeVerts, GL_STATIC_DRAW);
	const VerticesData3f vert4Colors(cubeColors, GL_STATIC_DRAW);
	const VerticesData2f cubeTexBuffer(cubeTexCoords, GL_STATIC_DRAW);
	const Indices cubeIndi(cubeIndices, GL_STATIC_DRAW);

	// Mesh objects generation
	const Mesh cube;

	// Initalization of VAOs
	cube.initialize(&cubeVBO, &cubeTexBuffer, &cubeIndi);
	cube.addColorAttr(vert4Colors);

	const Texture janFace(&TextureData{ 
		"D:/programowanie/MojeNowe/Gl_sandbox/Assets/Textures/mordy.png" });
	const Texture wood(&TextureData{
		"D:/programowanie/MojeNowe/Gl_sandbox/Assets/Textures/container.png"});

	// Projection matrix
	glm::mat4 projection;
	projection = glm::perspective(glm::radians(60.0f), (f32)wnd.width / wnd.height, 0.1f, 100.0f);
	shader.activate();
	shader.setMat4("projection", projection);

	// Specifying to which texture each sampler belongs to
	shader.setInt("texture0", 0);
	shader.setInt("texture1", 1);

	// Other setup
	glPointSize(5.0); //Just for for bigger vertices
	glEnable(GL_DEPTH_TEST);

	// Main loop
	while (!glfwWindowShouldClose(wnd.wndPtr))
	{
		getInput(wnd.wndPtr);
		const f32 timeStart = glfwGetTime();
		//Rendering:
		glClearColor(0.2f, 0.3f, 0.3f, 1.0f);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		const f32 yellow = (sin(timeStart * 5.0f) / 2.0f) + 0.5f;
		cube.activate();

		// Using second shader program, just for testing
		shader2.activate();
		shader2.setVec4("myColor", yellow, yellow, 0.0f, 1.0f);
		shader2.setMat4("projection", projection);
		f32 movePos = -1.5f * timeStart;
		glm::mat4 model = glm::mat4(1.0f);
		model = glm::translate(model, glm::vec3(0.0f, 0.0f, movePos));
		//model = glm::rotate(model, timeStart, glm::vec3(1.0f, 0.3f, 0.5f));

		// Setting up second camera
		glm::mat4 followerCamera;
		f32 distance = 10.0f - PosZ;
		f32 camX = sin(glfwGetTime()) * distance;
		f32 camZ = movePos + cos(glfwGetTime()) * distance;
		glm::vec3 follPos{ camX, 0.0f, camZ };
		glm::vec3 follTarget{ -1.2f, -0.2f, -0.75f };
		followerCamera = glm::lookAt(glm::vec3(camX, 0.0f, camZ), glm::vec3(0.0f, 0.0f, movePos), glm::vec3(0.0f, 1.0f, 0.0f));

		// Rest for second shader
		shader2.setMat4("model", model);
		shader2.setMat4("view", followerCamera);
		glDrawArrays(GL_TRIANGLES, 0, 36);

		// Textures activation
		wood.activate(GL_TEXTURE0);
		janFace.activate(GL_TEXTURE1);

		//View matrix
		glm::mat4 standardCamera = glm::mat4(1.0f);
		standardCamera = glm::translate(standardCamera, glm::vec3(0.0f, 0.0f, PosZ));
		
		shader.activate();
		shader.setMat4("view", followerCamera);

		for (auto cubePos : cubePositions)
		{
			glm::mat4 model = glm::mat4(1.0f);
			model = glm::translate(model, cubePos);
			model = glm::rotate(model, timeStart, glm::vec3(1.0f, 0.3f, 0.5f));
			shader.setMat4("model", model);
			glDrawArrays(GL_TRIANGLES, 0, 36);
		}
		//glDrawElements(GL_TRIANGLES, 36, GL_UNSIGNED_INT, 0);

		// End work
		glBindVertexArray(0);
		glfwPollEvents();
		glfwSwapBuffers(wnd.wndPtr);
	}

	glfwTerminate();
	return 0;
}

void framebufferSizeCallback(GLFWwindow* window, const s32 width, const s32 height)
{
	glViewport(0, 0, width, height);
}

void getInput(GLFWwindow *window)
{
	if (glfwGetKey(window, GLFW_KEY_ESCAPE) == GLFW_PRESS)
		glfwSetWindowShouldClose(window, true);
	if (glfwGetKey(window, GLFW_KEY_1) == GLFW_PRESS)
		glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
	if (glfwGetKey(window, GLFW_KEY_2) == GLFW_PRESS)
		glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	if (glfwGetKey(window, GLFW_KEY_3) == GLFW_PRESS)
		glPolygonMode(GL_FRONT_AND_BACK, GL_POINT);
	if (glfwGetKey(window, GLFW_KEY_S) == GLFW_PRESS)
		PosZ = PosZ - 0.05f;
	if (glfwGetKey(window, GLFW_KEY_W) == GLFW_PRESS)
		PosZ = PosZ + 0.05f;
}
