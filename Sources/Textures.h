#pragma once
//Abstraction for a "raw data", simply because we need it only once
struct TextureData
{
	explicit TextureData(const GLchar* imgPath);
	TextureData(const TextureData&) = delete;
	TextureData(const TextureData&&) = delete;
	TextureData& operator=(const TextureData&) = delete;
	TextureData& operator=(const TextureData&&) = delete;
	~TextureData();

	 int width{};
	 int height{};
	 int nrChannels{};
	 unsigned char* imgPtr;
};

//Texture is just and id referenece to actual data in GPU
//Constructor instead of init func to not lose reference by accidently init again
//!!!!WARNING::CONSTRUCTION/DESTRUCTION WILL ONLY TAKE AFFECT IF OPENGL CONTEXT EXIST!!!
struct Texture
{
	explicit Texture(TextureData* tData);
	Texture(const Texture&) = delete;
	Texture &operator=(const Texture&) = delete;
	Texture(Texture&& other) noexcept;
	Texture& operator=(Texture&& other) noexcept;
	~Texture();

	void release();
	void activate(const GLenum texUnit) const;
	uint32_t m_id{};
private:
	void setupTextureSettings();
};