#pragma once

//TODO: Make it more memory efficient - replace std::string
class Shader
{
public:
	u32 programID;

	Shader(const GLchar* vertexPath, const GLchar* fragmentPath)
	{
		std::ifstream vertFile;
		std::ifstream fragFile;

		std::stringstream vsstr;
		vertFile.open(vertexPath);
		vsstr << vertFile.rdbuf();
		std::string vertString = vsstr.str();
		const GLchar* vertCode = (GLchar*)vertString.data();

		std::stringstream fsstr;
		fragFile.open(fragmentPath);
		fsstr << fragFile.rdbuf();
		std::string fragString = fsstr.str();
		const GLchar* fragCode = (GLchar*)fragString.data();

		u32 vertex, fragment;

		// vertex shader compilation
		vertex = glCreateShader(GL_VERTEX_SHADER);
		glShaderSource(vertex, 1, &vertCode, nullptr);
		glCompileShader(vertex);
		checkCompileErrors(vertex, "VERTEX");

		// fragment shader compilation 
		fragment = glCreateShader(GL_FRAGMENT_SHADER);
		glShaderSource(fragment, 1, &fragCode, nullptr);
		glCompileShader(fragment);
		checkCompileErrors(fragment, "FRAGMENT");

		// shader program linkage
		programID = glCreateProgram();
		glAttachShader(programID, vertex);
		glAttachShader(programID, fragment);
		glLinkProgram(programID);
		checkCompileErrors(programID, "PROGRAM");

		// shaders can be deleted cause thay are linked to the program
		glDeleteShader(vertex);
		glDeleteShader(fragment);
	}

	__forceinline void activate() const
	{
		glUseProgram(programID);
	}

	__forceinline void setBool(const std::string_view name, const b32 value) const
	{
		glUniform1i(glGetUniformLocation(programID, name.data()), value);
	}

	__forceinline void setInt(const std::string_view name, const s32 value) const
	{
		glUniform1i(glGetUniformLocation(programID, name.data()), value);
	}

	__forceinline void setFloat(const std::string_view name, const f32 value) const
	{
		glUniform1f(glGetUniformLocation(programID, name.data()), value);
	}

	__forceinline void setMat4(const std::string_view name, const glm::mat4& mat) const
	{
		glUniformMatrix4fv(glGetUniformLocation(programID, name.data()), 1, GL_FALSE, &mat[0][0]);
	}

	__forceinline void setVec4(const std::string_view name, const glm::vec4& value) const
	{
		glUniform4fv(glGetUniformLocation(programID, name.data()), 1, &value[0]);
	}

	__forceinline void setVec4(const std::string_view name, const f32 x, 
		const f32 y, const f32 z, const f32 w) const
	{
		glUniform4f(glGetUniformLocation(programID, name.data()), x, y, z, w);
	}

	__forceinline void setVec3(const std::string_view name, const glm::vec3& value) const
	{
		glUniform3fv(glGetUniformLocation(programID, name.data()), 1, &value[0]);
	}
	__forceinline void setVec3(const std::string_view name, const f32 x, const f32 y, const f32 z) const
	{
		glUniform3f(glGetUniformLocation(programID, name.data()), x, y, z);
	}

private:
	void checkCompileErrors(const u32 shader, const std::string_view type) const
	{
		s32 success;
		byte info[1024];
		if (type != "PROGRAM")
		{
			glGetShaderiv(shader, GL_COMPILE_STATUS, &success);
			if (!success)
			{
				glGetShaderInfoLog(shader, 1024, NULL, info);
				throw std::runtime_error(info);
			}
		}
		else
		{
			glGetProgramiv(shader, GL_LINK_STATUS, &success);
			if (!success)
			{
				glGetProgramInfoLog(shader, 1024, NULL, info);
				throw std::runtime_error(info);
			}
		}
	}
};