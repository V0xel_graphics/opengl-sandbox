﻿#pragma once

struct Window
{
	explicit Window(const uint32_t w = 1920,
		const uint32_t h = 1080) :
		width(w),
		height(h)
	{
		//initalization of glfw
		glfwInit();
		glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
		glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 5);
		glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

		//window creation
		wndPtr = glfwCreateWindow(width, height, "OpenGL Sandbox", nullptr, nullptr);
		if (wndPtr == nullptr)
		{
			glfwTerminate();
			throw std::runtime_error("Failed to create a window");
		}
		glfwMakeContextCurrent(wndPtr); //creates openGL context
	}
	
	uint64_t width;
	uint64_t height;
	GLFWwindow* wndPtr;
};
