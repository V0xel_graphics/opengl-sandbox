#include  "pch.h"
#include  "Mesh.h"

/*						VBO abstraction implemetations				*/
VerticesData3f::VerticesData3f(const std::vector<glm::vec3>& pos, const GLenum type)
{
	glGenBuffers(1, &m_vbo);
	glBindBuffer(GL_ARRAY_BUFFER, m_vbo);
	glBufferData(GL_ARRAY_BUFFER, pos.size() * 3 * sizeof(GLfloat), pos.data(), type);
}

VerticesData3f::~VerticesData3f()
{
	release();
}

void VerticesData3f::release()
{
	glDeleteBuffers(1, &m_vbo);
	m_vbo = 0;
}

void VerticesData3f::activate(const uint32_t pos) const
{
	glBindBuffer(GL_ARRAY_BUFFER, m_vbo);
	glEnableVertexAttribArray(pos);
	glVertexAttribPointer(pos, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(uint32_t), (void*)0);
}

VerticesData2f::VerticesData2f(const std::vector<glm::vec2>& pos, const GLenum type)
{
	glGenBuffers(1, &m_vbo);
	glBindBuffer(GL_ARRAY_BUFFER, m_vbo);
	glBufferData(GL_ARRAY_BUFFER, pos.size() * 2 * sizeof(GLfloat), pos.data(), type);
}

VerticesData2f::~VerticesData2f()
{
	release();
}

void VerticesData2f::release()
{
	glDeleteBuffers(1, &m_vbo);
	m_vbo = 0;
}

void VerticesData2f::activate(const uint32_t pos) const
{
	glBindBuffer(GL_ARRAY_BUFFER, m_vbo);
	glEnableVertexAttribArray(pos);
	glVertexAttribPointer(pos, 2, GL_FLOAT, GL_FALSE, 2 * sizeof(uint32_t), (void*)0);
}

/*						EBO abstraction implemetations				*/
Indices::Indices(const std::vector<uint32_t>& indi, const GLenum type)
{
	glGenBuffers(1, &m_ebo);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_ebo);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, indi.size() * sizeof(uint32_t), indi.data(), type);
}

Indices::~Indices()
{
	release();
}

void Indices::release()
{
	glDeleteBuffers(1, &m_ebo);
	m_ebo = 0;
}

/*						VAO abstraction implemetations							*/
Mesh::Mesh()
{
	glGenVertexArrays(1, &m_vao);
}

Mesh::~Mesh()
{
	release();
}

void Mesh::initialize(const VerticesData3f* vertsPos, const VerticesData2f* textCoords, const Indices* indicies) const
{
	glBindVertexArray(m_vao);
	vertsPos->activate(VERT_POS);
	if (textCoords != nullptr) //Dirty just for now
		textCoords->activate(TEX_COORDS);
	indicies->activate();
	glBindVertexArray(0);
}

void Mesh::release()
{
	glDeleteVertexArrays(1, &m_vao);
	m_vao = 0;
}
