#include "pch.h"
#include "Textures.h"

TextureData::TextureData(const GLchar* imgPath)
{
	stbi_set_flip_vertically_on_load(true);
	imgPtr = stbi_load(imgPath,
	                   &width,
	                   &height,
	                   &nrChannels,
	                   0);
	if (!imgPtr)
		throw std::runtime_error("Failed to receive image");
}

TextureData::~TextureData()
{
	stbi_image_free(imgPtr);
}

Texture::Texture(TextureData* tData)
{
	setupTextureSettings();
	glGenTextures(1, &m_id);
	GLenum colorFormat = GL_RGB;
	if (tData == nullptr)
		throw std::runtime_error("Invalid data for texture provided");
	if (tData->nrChannels > 3)
		colorFormat = GL_RGBA;

	glBindTexture(GL_TEXTURE_2D, m_id);
	glTexImage2D(
		GL_TEXTURE_2D,
		0,
		colorFormat,
		tData->width,
		tData->height,
		0,
		colorFormat,
		GL_UNSIGNED_BYTE, 
		tData->imgPtr);
	glGenerateMipmap(GL_TEXTURE_2D); //For now lets just automatically generate mipmap
}

Texture::Texture(Texture&& other) noexcept: m_id(other.m_id)
{
	other.m_id = 0; //Use the "null" texture for the old object.
}

Texture& Texture::operator=(Texture&& other) noexcept
{
	if (this != &other)
	{
		release();
		//m_id is now 0.
		std::swap(m_id, other.m_id);
	}
	return *this;
} 
//If we lose reference then memory for it should be released from GPU
Texture::~Texture()
{
	release();
}

void Texture::release()
{
	glDeleteTextures(1, &m_id);
	m_id = 0;
}

void Texture::activate(const GLenum texUnit) const
{
	glActiveTexture(texUnit);
	glBindTexture(GL_TEXTURE_2D, m_id);
}

void Texture::setupTextureSettings()
{
	//Wrapping settings:
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	//Filtering settings:
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR); //for minifyiing and for mipmaps
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR); //for magnifying
}
