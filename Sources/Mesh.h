#pragma once
//TODO::Consider the activation functions, in reality they are not really needed, binds can be called directly
//For now, let restrict it to not be copyable, moveable etc.

//VBO abstraction for 3f
struct VerticesData3f
{
	friend struct Mesh;
	VerticesData3f(const std::vector<glm::vec3>& pos,
	               const GLenum type);
	VerticesData3f(const VerticesData3f&) = delete;
	VerticesData3f(const VerticesData3f&&) = delete;
	VerticesData3f& operator=(const VerticesData3f&) =  delete;
	VerticesData3f& operator=(const VerticesData3f&&) =  delete;
	~VerticesData3f();

	u32 m_vbo{};

	void release();

private:
	void activate(const u32 pos) const;
};

//VBO abstraction for 2f
struct VerticesData2f
{
	friend struct Mesh;
	VerticesData2f(const std::vector<glm::vec2>& pos,
	               const GLenum type);
	VerticesData2f(const VerticesData2f&) = delete;
	VerticesData2f(const VerticesData2f&&) = delete;
	VerticesData2f& operator=(const VerticesData2f&) = delete;
	VerticesData2f& operator=(const VerticesData2f&&) = delete;
	~VerticesData2f();

	u32 m_vbo{};

	void release();
private:
	void activate(const u32 pos) const;
};

//EBO abstraction //TODO::Maybe change it to be "mesh-only"?
struct Indices
{
	Indices(const std::vector<u32>& indi,
	        const GLenum type);
	Indices(const Indices&) = delete;
	Indices(const Indices&&) = delete;
	Indices& operator=(const Indices&) = delete;
	Indices& operator=(const Indices&&) = delete;
	~Indices();

	u32 m_ebo{};

	void activate() const
	{
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_ebo);
	}

	void release();
};

//VAO abstraction
struct Mesh
{
	Mesh();
	Mesh(const Mesh&) = delete;
	Mesh(const Mesh&&) = delete;
	Mesh& operator=(const Mesh&) = delete;
	Mesh& operator=(const Mesh&&) = delete;
	~Mesh();
	u32 m_vao{};

	//Has to be in shaders in a same way!
	#define VERT_POS   0
	#define COLOR_POS  1
	#define TEX_COORDS 2

	void initialize(
		const VerticesData3f* vertsPos,
		const VerticesData2f* textCoords,
		const Indices* indicies) const;

	void activate() const
	{
		glBindVertexArray(m_vao);
	}

	void release();

	//TODO::Just for now, remove it later!!
	void addColorAttr(const VerticesData3f& vertex) const
	{
		glBindVertexArray(m_vao);
		vertex.activate(COLOR_POS);
		glBindVertexArray(0);
	}
};
