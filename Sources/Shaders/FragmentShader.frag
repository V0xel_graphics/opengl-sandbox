#version 330 core
in vec3 vertColor;
in vec2 texCoord;

out vec4 FragColor;

uniform sampler2D texture0;
uniform sampler2D texture1;

void main()
{
	vec4  tex0Color = texture(texture0, texCoord);
	vec4 backColor = mix(tex0Color, vec4(vertColor, 1.0), tex0Color.a * 0.5);
	vec4 frontColor = texture(texture1, texCoord);
	FragColor = mix( backColor, frontColor, frontColor.a * 0.8);
} 